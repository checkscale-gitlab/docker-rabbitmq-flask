# Introduction
    This is a example of usage of a rabbitMQ dockarized application in python

# First should build the consumer and producer images
    cd producer
    docker build . -t producer
    cd ../consumer
    docker build . -t consumer
    cd ..
    
# Then should run the docker compose
    docker-compose up -d

# To insert data into the rabbitMQ run 
you can change the body of the json
    python3 send.py

# To get the data that is on MongoDB
    python3 list-mongodb-data.py
