#!/usr/bin/env python3

import requests

r = requests.post(
    "http://127.0.0.1:5000/rabbitmq",
    json={
        "user": "Rui",
        "message": "Ola"
    }
)

print(r.status_code, r.reason)
